locals {
  fullname = length(regexall("memcached", var.chart_name)) > 0 ? trimsuffix(substr(var.chart_name, 0, 63), "-") : trimsuffix(substr("${var.chart_name}-memcached", 0, 63), "-")
}

output "hostname" {
  value      = "${local.fullname}.${var.namespace}.svc.cluster.local"
  depends_on = [helm_release.memcached]
}
