variable "namespace" {
  type = string
}
variable "chart_name" {
  type = string
}
variable "chart_version" {
  type    = string
  default = "6.0.5"
}
variable "values" {
  type    = list(string)
  default = []
}
# tflint-ignore: terraform_unused_declarations
variable "image_repository" {
  type    = string
  default = "bitnami/memcached"
}
# tflint-ignore: terraform_unused_declarations
variable "image_tag" {
  type    = string
  default = "1.6.14-debian-10-r16"
}

variable "helm_force_update" {
  type    = bool
  default = false
}
variable "helm_recreate_pods" {
  type    = bool
  default = false
}
variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}
variable "helm_max_history" {
  type    = number
  default = 0
}

variable "limits_cpu" {
  type    = string
  default = "250m"
}
variable "limits_memory" {
  type    = string
  default = "256Mi"
}
variable "requests_cpu" {
  type    = string
  default = "250m"
}
variable "requests_memory" {
  type    = string
  default = "256Mi"
}
